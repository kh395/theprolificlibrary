//
//  BookDetailViewController.swift
//  TheProlificLibrary
//
//  Created by Kent Huang on 11/2/15.
//  Copyright © 2015 Kent Huang. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController {
    
    var book: Book!
    
    @IBOutlet weak var detailTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.detailTextView.editable = false
        self.setDetailViewText()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.refresh()
    }

    @IBAction func backButtonPressed(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func shareButtonPressed(sender: UIBarButtonItem) {
        let controller = UIActivityViewController(activityItems: ["I'm currently reading: ", self.book.title, "by \(self.book.author)"], applicationActivities: nil)
        navigationController?.presentViewController(controller, animated: true, completion: nil)
    }

    @IBAction func checkoutButtonPressed(sender: UIButton) {
        let alertController = UIAlertController(title: "Checkout", message: "Input your name", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Name"
        }
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (_) -> Void in
//            let formatter = NSDateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss zzz"
//            let date = formatter.stringFromDate(NSDate())
//            self.book.lastCheckedOut = date
            self.book.lastCheckedOutBy = alertController.textFields![0].text
            
            // PUT
            Networking.update(self.book, completionHandler: { self.refresh() })
        }
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func updateButtonPressed(sender: UIButton) {
        let addBookViewController = storyboard?.instantiateViewControllerWithIdentifier("AddBookViewController") as? AddBookViewController
        addBookViewController?.book = self.book
        if let addBookViewController = addBookViewController {
            self.presentViewController(addBookViewController, animated: true, completion: nil)
        }
    }
    
    
    func setDetailViewText() {
        self.detailTextView.text = "\t\(self.book.title!)\n" +
            "\t\(self.book.author!)\n" +
            "\tPublisher: \(self.book.publisher!)\n" +
            "\tCategories: \(self.book.categories!)\n"
        
        if let name = self.book.lastCheckedOutBy, time = self.book.lastCheckedOut {
            self.detailTextView.text = self.detailTextView.text + "\tLast Checked Out: \n" + "\t\(name) @ \(time)"
        }
    }
    
    func refresh() {
        Networking.getOne(self.book) { (book) -> Void in
            self.book = book!
            self.setDetailViewText()
        }
    }
    
    
}
