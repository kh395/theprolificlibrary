//
//  BooksTableViewController.swift
//  TheProlificLibrary
//
//  Created by Kent Huang on 11/2/15.
//  Copyright © 2015 Kent Huang. All rights reserved.
//

import UIKit

class BooksTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var booksTableView: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    var books = [Book]()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.navigationItem.rightBarButtonItem?.action = "editButtonItemPressed"
        
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: .ValueChanged)
        self.booksTableView.addSubview(self.refreshControl)
        self.booksTableView.alwaysBounceVertical = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.refresh()
    }

    // MARK: Begin TableView delegate and datasource methods
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let book = self.books[indexPath.row]
        let cell = self.booksTableView.dequeueReusableCellWithIdentifier("BookCell")!
        cell.textLabel!.text = book.title
        cell.detailTextLabel!.text = book.author
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.books.count
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if self.editing {
            return false
        } else {
            return true
        }
    }
    
    // Deleting a single book
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let alertController = UIAlertController(title: "Are you sure?", message: "Are you sure you want to delete this book?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                // DELETE
                Networking.deleteOne(self.books[indexPath.row], completionHandler: { self.refresh() })
            })
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    // disable Edit button if deleting a single book
    func tableView(tableView: UITableView, willBeginEditingRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationItem.rightBarButtonItem?.enabled = false
    }
    
    func tableView(tableView: UITableView, didEndEditingRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationItem.rightBarButtonItem?.enabled = true
    }
    
    // MARK: End TableView delegate and datasource methods
    
    // segue to detail
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let bookDetailViewController = segue.destinationViewController as! BookDetailViewController
            if let indexPath = self.booksTableView.indexPathForSelectedRow {
                bookDetailViewController.book = self.books[indexPath.row]
            }
        }
    }
    
    func refresh() {
        // GET
        Networking.getAll { (books) -> Void in
            self.books = books!
            self.booksTableView.reloadData()
            self.refreshControl.endRefreshing()
            
            self.editing = false
            self.toolbar.hidden = true
        }
    }
    
    func editButtonItemPressed() {
        if !self.editing {
            self.toolbar.hidden = false
            self.editing = true
        } else {
            self.toolbar.hidden = true
            self.editing = false
        }
    }
    
    @IBAction func deleteAllButtonPressed(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: "Are you sure?", message: "Are you sure you want to delete all books?", preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
            // DELETE ALL
            Networking.deleteAll({ self.refresh() })
        })
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}

