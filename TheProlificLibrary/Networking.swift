//
//  Networking.swift
//  TheProlificLibrary
//
//  Created by Kent Huang on 11/3/15.
//  Copyright © 2015 Kent Huang. All rights reserved.
//

import Foundation

class Networking: NSObject {
    
    static let BaseURL = "https://prolific-interview.herokuapp.com/56376f14b82f7d00098e59a8"
    
        enum Methods: String {
            case GET = "GET"
            case POST = "POST"
            case PUT = "PUT"
            case DELETE = "DELETE"
        }
    
    class func getAll(completionHandler: (books: [Book]?) -> Void) {
        let session = NSURLSession.sharedSession()
        let urlString = Networking.BaseURL + "/books"
        let url = NSURL(string: urlString)!
        let request = NSURLRequest(URL: url)
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if let error = error {
                print("Error requesting data: \(error)")
            } else {
                do {
                    let parsedResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [[String: AnyObject]]
                    var books = [Book]()
                    for each in parsedResult {
                        let book = Book(initWithDictionary: each)
                        books.append(book)
                    }
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completionHandler(books: books)
                    })
                } catch let error as NSError {
                    print("Error parsing JSON: \(error)")
                }
            }
        }
        task.resume()
    }
    
    class func getOne(book: Book, completionHandler: (book: Book?) -> Void) {
        let session = NSURLSession.sharedSession()
        let urlString = Networking.BaseURL + book.url!
        let url = NSURL(string: urlString)!
        let request = NSURLRequest(URL: url)
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if let error = error {
                print("Error requesting data: \(error)")
            } else {
                do {
                    let parsedResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [String: AnyObject]
                    let updatedBook = Book(initWithDictionary: parsedResult)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        completionHandler(book: updatedBook)
                    })
                } catch let error as NSError {
                    print("Error parsing JSON: \(error)")
                }
            }
        }
        task.resume()
    }
    
    class func post(book: Book) {
        Networking.genericHTTP(book, method: Networking.Methods.POST, urlParam: "/books/", completitionHandler: {})
    }

    class func update(book: Book, completionHandler: () -> Void) {
        Networking.genericHTTP(book, method: Networking.Methods.PUT, urlParam: book.url!, completitionHandler: completionHandler)
    }
    
    class func deleteOne(book: Book, completionHandler: () -> Void) {
        Networking.genericHTTP(nil, method: Networking.Methods.DELETE, urlParam: book.url!, completitionHandler: completionHandler)
    }
    
    class func deleteAll(completionHandler: () -> Void) {
        Networking.genericHTTP(nil, method: Networking.Methods.DELETE, urlParam: "/clean", completitionHandler: completionHandler)
    }
    
    
    class func genericHTTP(book: Book?, method: Networking.Methods, urlParam: String, completitionHandler: () -> Void) {
            
        let session = NSURLSession.sharedSession()
        let urlString = Networking.BaseURL + urlParam
        let url = NSURL(string: urlString)!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "\(method)"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        // POST and PUT requires HTTPBody
        if method == Networking.Methods.POST || method == Networking.Methods.PUT {
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(book!.toDict(), options: NSJSONWritingOptions.PrettyPrinted)
            } catch let error as NSError {
                print("Error parsing JSON: \(error)")
            }
        }
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            if let data = data, response = response {
                print("RESPONSE: \(response)")
                print("DATA: " + String(data: data, encoding: NSUTF8StringEncoding)!)
                completitionHandler()
            } else {
                print("Error requesting data: \(error!)")
            }
        }
        task.resume()
    }
    
}