//
//  Book.swift
//  TheProlificLibrary
//
//  Created by Kent Huang on 11/2/15.
//  Copyright © 2015 Kent Huang. All rights reserved.
//

import Foundation

class Book: NSObject {
    var author: String!
    var categories: String!
    var title: String!
    var publisher: String!
    
    var lastCheckedOut: String?
    var lastCheckedOutBy: String?
    var id: Int?
    var url: String?
    
    // Used with AddBookViewController
    init(_ author: String, _ categories: String, _ title: String, _ publisher: String) {
        self.author = author
        self.categories = categories
        self.title = title
        self.publisher = publisher
    }
    
    // Used when retrieved via GET method
    init(initWithDictionary dictionary: [String: AnyObject]) {
        self.author = dictionary["author"] as? String
        self.categories = dictionary["categories"] as? String
        self.title = dictionary["title"] as? String
        self.publisher = dictionary["publisher"] as? String
        self.lastCheckedOut = dictionary["lastCheckedOut"] as? String
        self.lastCheckedOutBy = dictionary["lastCheckedOutBy"] as? String
        self.id = dictionary["id"] as? Int
        self.url = dictionary["url"] as? String
    }
    
    func toDict() -> [String: AnyObject] {
        var bookDict = [String: AnyObject]()
        bookDict["author"] = self.author
        bookDict["categories"] = self.categories
        bookDict["title"] = self.title
        bookDict["publisher"] = self.publisher
        if let lastCheckedOutBy = self.lastCheckedOutBy {
            bookDict["lastCheckedOutBy"] = lastCheckedOutBy
        }
        return bookDict
    }
    
}

