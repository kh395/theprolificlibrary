//
//  AddBookViewController.swift
//  TheProlificLibrary
//
//  Created by Kent Huang on 11/2/15.
//  Copyright © 2015 Kent Huang. All rights reserved.
//

import UIKit

class AddBookViewController: UIViewController, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var authorTextField: UITextField!
    @IBOutlet weak var publisherTextField: UITextField!
    @IBOutlet weak var categoriesTextField: UITextField!
    
    var book: Book? //if not nil, then update the book. if nil, then create new book
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scrollView.scrollEnabled = false
        
        self.titleTextField.delegate = self
        self.authorTextField.delegate = self
        self.publisherTextField.delegate = self
        self.categoriesTextField.delegate = self
        
        if let book = self.book {
            self.titleTextField.text = book.title
            self.authorTextField.text = book.author
            self.publisherTextField.text = book.publisher
            self.categoriesTextField.text = book.categories
        }
    }

    @IBAction func doneButtonPressed(sender: UIBarButtonItem) {
        
        guard let title = self.titleTextField.text else { return }
        guard let author = self.authorTextField.text else { return }
        guard let publisher = self.publisherTextField.text else { return }
        guard let categories = self.categoriesTextField.text else { return }
        
        if !(title.isEmpty && author.isEmpty && publisher.isEmpty && categories.isEmpty) {
            let alertController = UIAlertController(title: "Are you sure?", message: "Are you sure you want to leave the page without saving?", preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (_) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            alertController.addAction(okAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func submitButtonPressed(sender: UIButton) {
        
        guard let title = self.titleTextField.text else { return }
        guard let author = self.authorTextField.text else { return }
        guard let publisher = self.publisherTextField.text else { return }
        guard let categories = self.categoriesTextField.text else { return }

        if title.isEmpty || author.isEmpty || publisher.isEmpty || categories.isEmpty {
            let alertController = UIAlertController(title: "Error", message: "One or more fields have not yet been filled.", preferredStyle: UIAlertControllerStyle.Alert)
            let defaultAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            if let book = self.book {
                book.title = title
                book.author = author
                book.publisher = publisher
                book.categories = categories
                // PUT
                Networking.update(book, completionHandler: {})
            } else {
                let newBook = Book(author, categories, title, publisher)
                // POST
                Networking.post(newBook)
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.scrollView.scrollEnabled = true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
